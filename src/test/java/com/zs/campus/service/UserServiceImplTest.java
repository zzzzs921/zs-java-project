package com.zs.campus.service;

import com.zs.campus.bean.User;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author 张硕
 * @version 1.0
 */
public class UserServiceImplTest {
    private ApplicationContext ioc;
    private UserService userService;
    @Before
    public void init(){
        ioc = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过类型获取furnService接口对象/代理对象
        userService = ioc.getBean(UserService.class);
    }


//    @Test
//    public void test(){
//        User user = userService.findUserByNameAndPwd("admin", "admin");
//        System.out.println(user);
//    }

    @Test
    public void add(){
        User user = new User();
        user.setId(2);
        user.setName("jack");
        user.setPwd("jack");
        userService.addUser(user);
    }
}
