<%--
  Created by IntelliJ IDEA.
  User: 张硕
  Date: 2024/7/29
  Time: 19:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Test</title>

    <script type="text/javascript" src="/js/jquery-3.7.1.min.js"/>
    <script>
        $(function(){
            $("#info").val(${requestScope.ok})
        })
    </script>
</head>

<body>
<h1>注册用户</h1>
<form action="addUser">
    学　号<input type="text" name="id"><br/>
    用户名<input type="text" name="name"><br/>
    密　码<input type="password" name="pwd">
    <br/>
    <input  type="submit"  value="注册" />
    <input type="reset" value="重置"/><input style="border: 0px" id="info">
    <br/>
    <a href="../index.jsp">返回登录页</a>
</form>
</body>
</html>
