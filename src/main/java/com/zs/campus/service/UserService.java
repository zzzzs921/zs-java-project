package com.zs.campus.service;

import com.zs.campus.bean.User;

/**
 * @author 张硕
 * @version 1.0
 */
public interface UserService {

    //添加用户
    void addUser(User user);

    //根据名字和密码，查找对应的对象，找到就进入页面，否则提示错误停在登录页

}
