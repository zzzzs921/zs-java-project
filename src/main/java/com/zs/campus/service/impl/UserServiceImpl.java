package com.zs.campus.service.impl;

import com.zs.campus.bean.User;
import com.zs.campus.bean.UserExample;
import com.zs.campus.dao.UserMapper;
import com.zs.campus.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 张硕
 * @version 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public void addUser(User user) {
        userMapper.insert(user);
    }

//    @Override
//    public User findUserByNameAndPwd(String name,String pwd) {
//        UserExample userExample = new UserExample();
//        UserExample.Criteria criteria = userExample.createCriteria();
//        criteria.andNameEqualTo(name);
//        criteria.andPwdEqualTo(pwd);
//        List<User> users = userMapper.selectByExample(userExample);
//        if (users == null){
//            return null;
//        }else {
//            return new User();
//        }
//    }
}
