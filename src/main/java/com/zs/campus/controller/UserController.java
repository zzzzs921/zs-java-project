package com.zs.campus.controller;

import com.zs.campus.bean.User;
import com.zs.campus.service.UserService;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author 张硕
 * @version 1.0
 */
@Controller
@RequestMapping("/first")
public class UserController {
    @Resource
    private UserService userService;

    @RequestMapping("/addUser")

    public String addUser(HttpServletRequest request, String name, String pwd, String id) {
        User user = new User();
        user.setName(name);
        user.setPwd(pwd);
        user.setId(Integer.parseInt(id));
        userService.addUser(user);
        request.setAttribute("ok","注册成功");
            return "/first/register";
    }

//    @RequestMapping("/hi")
//    public String findUserByNameAndPwd(@RequestParam String name, String pwd){
//        User user = userService.findUserByNameAndPwd(name, pwd);
//        if (user == null){
//            return "error";
//        }else {
//            return "hi";
//        }
//    }

}
